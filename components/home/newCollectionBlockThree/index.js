import Link from 'next/link'
import NewColTwoCSS from './NewCollectionBlockThree.module.css'
import Image from 'next/image'

const NewCollectionBlockThree = ({item}) => {
    return(
         <div className={NewColTwoCSS.collection}>
             {item ?
            <div className="container">
                <div className={NewColTwoCSS.collection__blocks}>

                    <div className={NewColTwoCSS.collection__blocks_image}>
                        <img src={item.image} alt="img2"/>
                    </div>
                    <div className={NewColTwoCSS.collection__blocks_text}>
                        <div>
                            <h1>{item.title}</h1>
                        </div>
                        <div>
                            <Link href={item.url}>
                                <a>{item.button_title}</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
                 : null}
        </div>

    )
}

export default NewCollectionBlockThree