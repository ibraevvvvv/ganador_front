
import newCollBlockCSS from './NewCollectionBlock.module.css'
import {useSelector} from "react-redux";
import Link from 'next/link';
import Image from 'next/image'

const NewCollectionBlock = () => {
    const mainBanner = useSelector(({mainPage}) => mainPage.items.main_banner );
    return(
        <div className={newCollBlockCSS.collection}>
            <div className="container">
                <div className={newCollBlockCSS.collection__blocks}>
                    <div className={newCollBlockCSS.collection__blocks_text}>
                        <div>
                            <h1>{mainBanner?.title}</h1>
                        </div>
                        <div>
                            <Link href={mainBanner?.url || ''}>
                                 <a>{mainBanner?.button_title}</a>
                            </Link>
                        </div>
                    </div>
                    <div className={newCollBlockCSS.collection__blocks_image}>
                        <img src={mainBanner?.image} alt='main-pic'/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewCollectionBlock