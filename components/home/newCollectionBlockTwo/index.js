import Link from 'next/link'
import NewColTwoCSS from './NewCollectionBlockTwo.module.css'

const NewCollectionBlockTwo = ({item}) => {
    return(
        <div className={NewColTwoCSS.collection}>
            <div className="container">
                <div className={NewColTwoCSS.collection__blocks}>
                    <div className={NewColTwoCSS.collection__blocks_text}>
                        <div>
                            <h1>{item.title}</h1>
                        </div>
                        <div>
                            <Link href={item.url}>
                                <a>{item.button_title}</a>
                            </Link>
                        </div>
                    </div>
                    <div className={NewColTwoCSS.collection__blocks_image}>
                        <img src={item?.image} alt="img2"/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default NewCollectionBlockTwo