import Link from 'next/link'
import CatItem from './CategoriesItem.module.css'
import Image from 'next/image'

const CategoriesItem = ({categoryId}) => {
    return(
        <Link href={`/clothes/${categoryId.id}`} passHref>
        <div className={CatItem.clothes__item}>
            <div className={CatItem.clothes__item_image}>
                <img src={categoryId?.image} alt={"cat-img"}/>
            </div>
            <div className={CatItem.clothes__item__text}>
                <h5 className={CatItem.h1}>{categoryId?.title}</h5>
                <p>{categoryId?.short_description}</p>
                <div className={CatItem.price_wrapper}>
                    <div>
                        <p className={CatItem.old_price}>{categoryId?.old_price} p.</p>
                    </div>
                    <div>
                        <p className={CatItem.new_price}>{categoryId?.actual_price} p.</p>
                    </div>
                </div>
            </div>
        </div>
    </Link>
    )
}

export default CategoriesItem