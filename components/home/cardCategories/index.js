import Link from 'next/link'
import CardCatCSS from './CardCategories.module.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {useSelector} from "react-redux";
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import SwiperCore, { Navigation, Pagination} from 'swiper';
SwiperCore.use([Navigation, Pagination]);

const CardCategories = () => {
    const slider = useSelector(({mainPage}) => mainPage.items.slider);
    return(
        <div className={CardCatCSS.cardCategories}>
            <div className="container">
                <Swiper
                    navigation={true}
                    pagination={{ clickable: true }}
                    preloadImages={true}
                    spaceBetween={50}
                    breakpoints={{
                        1400: {
                            slidesPerView: 4,
                        },
                        1300: {
                            slidesPerView: 3,
                        },
                        1200: {
                            slidesPerView: 3,
                        },
                        1100: {
                            slidesPerView: 3,
                        },
                        1024: {
                            slidesPerView: 3,
                        },
                        900: {
                            slidesPerView: 3,
                        },
                        768: {
                            slidesPerView: 2,
                        },
                        630: {
                            slidesPerView: 2,
                        },
                        425: {
                            slidesPerView: 1,
                            spaceBetween: 50
                        },
                        375: {
                            slidesPerView: 1,
                            spaceBetween: 50
                        },
                        320: {
                            slidesPerView: 1,
                            spaceBetween: 20
                        }
                    }}
                >
                    {slider?.map((item) => (
                        <SwiperSlide key={item.id}>
                            <div className={CardCatCSS.card__blocks__item}>
                                <img src={item.image} alt={"imageCard1"}/>
                                <Link href={`/category/${item.slug}`} passHref>
                                    <div className={CardCatCSS.card__blocks__item_inner} >
                                        <a>{item.title}</a>
                                    </div>
                                </Link>
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
               
                    
            </div>
        
      </div>
    )
}

export default CardCategories