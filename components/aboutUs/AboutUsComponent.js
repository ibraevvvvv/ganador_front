import React,{useEffect} from "react"
import AboutUsCSS from './AboutUsComponent.module.css'
import FirstBlock from './firstBlock'
import OurServices from './ourServices'

const AboutUsComponent = () => {
    return(
            <div className={AboutUsCSS.aboutUs}>
                <div className="container">
                    <FirstBlock />
                    <OurServices />
                </div>
            </div>
    )
}

export default AboutUsComponent