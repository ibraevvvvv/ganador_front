import AboutUsCSS from '../AboutUsComponent.module.css'
import {useSelector} from "react-redux";
import Image from 'next/image'

const FirstBlock = () => {
    const items = useSelector(({about}) => about.items );
    return(
            <div className={AboutUsCSS.first_block}>
                <div className={AboutUsCSS.first_block__inner}>
                    <img src={items?.block_one_image || ''} alt={"about-img"} />
                </div>
                <div className={AboutUsCSS.first_block__inner}>
                    <h1>{items.block_one_title}</h1>
                    <p dangerouslySetInnerHTML={{ __html: items.block_one_body }}>
                    </p>
                </div>
            </div>
    )
}

export default FirstBlock