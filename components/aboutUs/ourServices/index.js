// import AboutUsCSS from '../AboutUsComponent.module.css'
// import {useSelector} from "react-redux";
// import Link from 'next/link';
// import Image from 'next/image'
// const OurServices = () => {
//     const items = useSelector(({about}) => about.items );
//     return(
//             <div className={AboutUsCSS.our_services}>
//                 <h1>Наши услуги</h1>
//                 <div className={AboutUsCSS.our_services__container}>
//                     <div className={AboutUsCSS.our_services__blocks}>
//                         <div>
//                             <h2>{items.block_two_title}</h2>
//                             <p dangerouslySetInnerHTML={{ __html: items.block_two_body }}>
//                             </p>
//                         </div>
//                         <div className={AboutUsCSS.block_two_image}>
//                             <Link href={items.block_two_url || ''}>
//                                 <a>
//                                     <img src={items?.block_two_image} alt={"block_two_image"}/>
//                                 </a>
//                             </Link>
//                         </div>
//                     </div>
//                     <div className={AboutUsCSS.our_services__blocks}>
//                         <h2>{items.block_three_title}</h2>
//                         <p dangerouslySetInnerHTML={{ __html: items.block_three_body }}>
//                         </p>
//                         <div className={AboutUsCSS.block_two_image}>
//                             <Link href={items.block_two_url || ''}>
//                                 <a>
//                                     <img src={items?.block_three_image} alt={"block_three_image"}/>
//                                 </a>
//                             </Link>
//                         </div>
//
//                     </div>
//                 </div>
//
//             </div>
//     )
// }
//
// export default OurServices

import AboutUsCSS from '../AboutUsComponent.module.css'
import {useSelector} from "react-redux";
import Link from 'next/link';
import Image from 'next/image'
const OurServices = () => {
    const items = useSelector(({about}) => about.items );
    return(
        <div className={AboutUsCSS.our_services}>
            <h1>Наши услуги</h1>
            <div className={AboutUsCSS.our_services__container}>
                <div className={AboutUsCSS.our_services__blocks}>
                    <div className={AboutUsCSS.our_services__block}>
                        <h2>{items.block_two_title}</h2>
                        <p dangerouslySetInnerHTML={{ __html: items.block_two_body }}>
                        </p>
                    </div>
                    <div className={AboutUsCSS.our_services__block}>
                        <h2>{items.block_three_title}</h2>
                        <p dangerouslySetInnerHTML={{ __html: items.block_three_body }}>
                        </p>
                    </div>
                </div>
                <div className={AboutUsCSS.our_services__blocks}>
                    <div className={AboutUsCSS.our_services__block}>
                        <Link href={items.block_two_url || ''}>
                            <a>
                                <img src={items?.block_two_image} alt={"block_two_image"}/>
                            </a>
                        </Link>
                    </div>
                    <div className={AboutUsCSS.our_services__block}>
                        <Link href={items.block_three_url || ''}>
                            <a>
                                <img src={items?.block_three_image} alt={"block_three_image"}/>
                            </a>
                        </Link>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default OurServices