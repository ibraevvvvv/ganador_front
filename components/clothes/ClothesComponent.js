import classNames from 'classnames'
import React, {useState} from 'react'
import clothesCSS from './ClothesComponent.module.css'
import {useSelector, useDispatch} from "react-redux";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMinus, faPlus} from "@fortawesome/free-solid-svg-icons";
import Slider from 'react-slick';
import Image from 'next/image'

const ClothesComponent = () => {
    const clothesId = useSelector(({clothesId}) => clothesId.items);
    // const [descShow, setdescShow] = React.useState({
    //     index: null,
    //     flag: false
    // })
    const [additions, setAdditions] = useState(JSON.parse(JSON.stringify(clothesId.additions.map(item => ({
        ...item,
        active: false
    }))) || []))

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };
    console.log(additions)
    const showDesc = (e, index) => {
        e.stopPropagation()
        setAdditions(old => {
            let newArr = [...old]
            newArr[index] = {...old[index], active: !old[index].active}
            return newArr
        })
        // setdescShow(old => ({...old, index: index, flag: !old.flag}))
    }
    return (
        <div className="container">
            {
                clothesId &&
                <div className={clothesCSS.detailClothesWrapper}>
                    <div className={clothesCSS.detailClothesSlider}>
                        <Slider {...settings}>
                            {clothesId?.images?.map((item, index) => (
                                <img src={item} alt='slider-clothes' key={index}/>
                            ))}

                        </Slider>
                    </div>
                    <div className={clothesCSS.detailClothesContent}>
                        <div className={clothesCSS.detailClothesTitle}>
                            <h1>{clothesId.title}</h1>
                        </div>
                        <div className={clothesCSS.detailClothesDescription}>
                            <span>{clothesId.description}</span>
                        </div>
                        <div className={clothesCSS.detailClothesPrice}>
                            <div
                                className={classNames(clothesCSS.detailClothesPrice, clothesCSS.child1)}>{clothesId?.old_price} p.
                            </div>
                            <div
                                className={classNames(clothesCSS.detailClothesPrice, clothesCSS.child2)}>{clothesId?.actual_price} p.
                            </div>
                        </div>
                        <div className={clothesCSS.detailClothesSizes}>
                            {
                                clothesId.sizes?.map((item, index) => (
                                    <div key={index}><span className={clothesCSS.detailClothesSizesInner}>{item}</span>
                                    </div>
                                ))
                            }
                        </div>
                        <div className={clothesCSS.detailClothesAccordeon}>
                            {additions?.map((item, index) => (
                                <div className={clothesCSS.detailClothesAccordeonItem} key={index}>
                                    <div className={clothesCSS.detailClothesAccordeonItemTitle}
                                         onClick={(e) => showDesc(e, index)}>
                                        <div><p>{item.title}</p></div>
                                        <div>
                                            <i className={item.active ? "fas fa-plus" : "fas fa-minus"}>
                                                <FontAwesomeIcon width="15px"
                                                                 icon={item.active ? faMinus : faPlus}/>
                                            </i>
                                        </div>
                                    </div>
                                    <div className={classNames(item.active
                                        ?
                                        (clothesCSS.detailClothesAccordeonDescription, clothesCSS.active)
                                        : clothesCSS.detailClothesAccordeonDescription)}>
                                        <span dangerouslySetInnerHTML={{__html: item.body}}/>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            }
        </div>

    )
}

export default ClothesComponent