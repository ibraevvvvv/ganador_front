import React from 'react'
import Link from 'next/link'
import NavbarCSS from './Navbar.module.css'
import classNames from 'classnames/bind';
import {useSelector,useDispatch} from "react-redux";
import Image from 'next/image'

const Navbar = ({setShowCategories,showCategories}) => {

    const dispatch = useDispatch()
    const category = useSelector(({category:{items}})=>items)
    const social = useSelector(({social}) => social.items );
    const contacts = useSelector(({contacts}) => contacts.items );
    const categoryHandler = title =>{
        setShowCategories(!showCategories)
        dispatch({type:"SET_CATEGORY", payload:category.filter(item=>item.title===title)})}

    return (
        <div className="container">
            <nav className={NavbarCSS.navbar}>
            <div className={NavbarCSS.navbar__left}>
                <div className={NavbarCSS.navbar__left_item}>
                    <Link href={"/"}>
                        <img src="/images/ganador_logo.png" alt='Ganador'/>
                    </Link>
                </div>
                {category && category.map((item, index) => (
                    <div className={NavbarCSS.navbar__left_item} key={index}>
                        <a onClick={() => categoryHandler(item.title)}>{item.title}</a>
                     </div> 
                ))}
                <div className={NavbarCSS.navbar__left_item}>
                    <Link href={"/about-us"} passHref>
                         <a href="#">О нас</a>
                    </Link>
                </div>
                <div className={NavbarCSS.navbar__left_item}>
                    <Link href={"/contacts"} passHref>
                        <a href="#">Контакты</a>
                    </Link>
                  
                </div>
                <div className={classNames(NavbarCSS.navbar__left_item,NavbarCSS.black)}>
                    <a href={social?.whatsapp}>Позвоните мне</a>
                </div>
            </div>
            <div className={NavbarCSS.navbar__right}>
                <a href={`tel:${contacts?.phone1}`}  className={NavbarCSS.navbar__right_item}>
                    <span>{contacts?.phone1}</span>
                </a>
                <a href={`tel:${contacts?.phone2}`}  className={NavbarCSS.navbar__right_item}>
                    <span>{contacts?.phone2}</span>
                </a>
            </div>
        </nav>
        </div>
    )
}

export default Navbar