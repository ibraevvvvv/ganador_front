
import React from 'react'
import Link from 'next/link'
import CatListCSS from './CategoriesList.module.css'
import {useSelector,useDispatch} from "react-redux";
import { fetchCategoryId } from '../../../store/actions/categoryId.action';
import Image from 'next/image'
const CategoriesList = ({showCategories,setShowCategories}) => {
    const items = useSelector(({category}) => category.selectedItems );
    const dispatch = useDispatch()

    const onFetchSubCat = (item) => {
        setShowCategories(!showCategories)
        dispatch(fetchCategoryId(item.slug))
    }

    return (
        <div className={showCategories ? CatListCSS.CatListH1 : CatListCSS.CatListH2}>
            <div className="container">
                <div className={CatListCSS.categories__list_wrapper}>
                        <div className={CatListCSS.categories__list_block}>
                            {items[0]?.categories.map((item, index) => (
                                <div className={CatListCSS.categories__list_item} key={index}>
                                <Link href={`/category/${item.slug}`}>
                                    <a onClick={() => onFetchSubCat(item)}>{item.title}</a>
                                </Link>
                            </div>
                            ))}
                        </div>
                        <div className={CatListCSS.categories__list_block}>
                            <Link href={items[0]?.url || ''}>
                                <img src={items[0]?.image} alt='catList'/>
                            </Link>
                        </div>
                    </div>
            </div>
        </div>
    )
}

export default CategoriesList