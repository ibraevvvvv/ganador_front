import React, {useState, useEffect} from "react";
import NavMobCSS from './NavbarMobile.module.css'
import { faBars,faChevronLeft,faChevronRight,faTimes  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from 'next/link'
import classNames from 'classnames/bind';
import {useSelector,useDispatch} from "react-redux";
import {useRouter} from "next/router";
import { fetchCategoryId } from "../../../store/actions/categoryId.action";
import Image from 'next/image'

const NavbarMobile = () => {
    const category = useSelector(({category:{items}})=>items)
    const subcategory = useSelector(({category:{selectedItems}})=>selectedItems)
    const contacts = useSelector(({contacts}) => contacts.items );
    const router = useRouter()

    let [openBurgerMenu, setOpenBurgerMenu] = useState(false);
    let [showCategories, setShowCategories] = useState(false);

    const closeAll = () => {
        setOpenBurgerMenu(!openBurgerMenu)
        setShowCategories(false)
    }

    const dispatch = useDispatch()

    const categoryHandler = title =>{
        setShowCategories(!showCategories)
        dispatch({type:"SET_CATEGORY", payload:category.filter(item=>item.title===title)})}
    
        const onFetchSubCat = (item) => {
                router.push({
                    pathname: `/category/${item.slug}`,
                }, undefined, {shallow: false})
        
            closeAll()
            dispatch(fetchCategoryId(item.slug))
        }

  return (
    <>
        <div className="container">
            <div className={NavMobCSS.navbarMobile_wrapper}>
                <div className={NavMobCSS.navbarMobile_left} 
                    onClick={closeAll}
                >
                    <i className={openBurgerMenu ? "fas fa-times" : "fas fa-bars "}>
                        <FontAwesomeIcon width="20px" icon={openBurgerMenu ? faTimes : faBars} />
                    </i>
                </div>
                <div className={NavMobCSS.navbarMobile_right}>
                    <Link href={"/"}>
                                <Image
                                   width={'188px'}
                                   height={'32px'}
                                    src="/images/ganador_logo.png"
                                    alt="Ganador"
                                />
                    </Link>
                </div>
            </div>
        </div>
        <div className={openBurgerMenu ? NavMobCSS.navbarMobile_list_active : NavMobCSS.navbarMobile_list}>
            <div className="container">
                <div className={NavMobCSS.navbarMobile_block}>
                    {category.map((item, index) => (
                        <div className={NavMobCSS.navbarMobile_item}
                            key={index}
                            onClick={() => setShowCategories(!showCategories)}
                        >   
                            <div className={NavMobCSS.navbarMobile_item_inner} onClick={() => categoryHandler(item.title)}>
                                    <div>
                                    <p>{item.title}</p>
                                    </div>
                                    <div>
                                        <i className="fas fa-chevron-right">
                                            <FontAwesomeIcon width="10px" icon={faChevronRight} />
                                        </i>
                                    </div>
                            </div>
                        
                        </div>
                        ))}
                    <div className={NavMobCSS.navbarMobile_item}>
                        <Link href={"/about-us"}>
                            <a href="#">О нас</a>
                        </Link>
                    </div>
                    <div className={NavMobCSS.navbarMobile_item}>
                        <Link href={"/contacts"}>
                            <a href="#">Контакты</a>
                        </Link>
                    </div>
                    <div className={classNames(NavMobCSS.navbarMobile_item,NavMobCSS.black)}>
                         <a href={`tel:${contacts.phone1}`}>Позвоните мне</a>
                    </div>
                </div>
            </div>
            
        </div>
        <div className={showCategories ? NavMobCSS.navbarMobile_list_active : NavMobCSS.navbarMobile_list}>
            <div className="container">
                <div className={NavMobCSS.navbarMobile_block}>
                     <div className={classNames(NavMobCSS.navbarMobile_item, NavMobCSS.back)}
                         onClick={() => setShowCategories(!showCategories)}
                    >   <div className={NavMobCSS.navbarMobile_item_back}>
                            <div>
                                <i className="fas fa-chevron-left">
                                    <FontAwesomeIcon width="10px" icon={faChevronLeft} />
                                </i>
                            </div>
                            <div>
                                <p>Назад</p>
                            </div>
                        </div>
                    </div>
                    {
                        subcategory[0]?.categories.map((item, index) => (
                            <div className={NavMobCSS.navbarMobile_item} key={index}>
                                    <a onClick={() => onFetchSubCat(item)}>{item.title}</a>
                            </div>
                        ))
                    }
                </div>
            </div>
            
        </div>
    </>
      
  );
};

export default NavbarMobile;

