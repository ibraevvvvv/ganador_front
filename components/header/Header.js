import React,{useEffect} from 'react'
import CategoriesList from './categoriesList'
import Navbar from './navbar/Navbar'
import NavbarMobile from './navbarMobile/NavbarMobile'
import {useSelector,useDispatch} from "react-redux";
import headerCSS from './Header.module.css'
import { fetchCategories } from '../../store/actions/categories.action';

const Header = () => {
    const [showCategories, setShowCategories] = React.useState(true)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCategories())
    },[]);

    return(
        <header className={headerCSS.header}>
            <Navbar
                showCategories={showCategories}
                setShowCategories={setShowCategories}
            />
            <CategoriesList
                showCategories={showCategories}
                setShowCategories={setShowCategories}
            />
            <NavbarMobile />
        </header>
    )
}

export default Header