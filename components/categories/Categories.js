import React from "react"
import CatCSS from './Categories.module.css'
import CategoriesItem from "../home/categoriesItem"
const Categories = ({categoryId}) => {
    return(
        <div className={CatCSS.clothes__wrapper}>
            {
                categoryId?.map((item, index) => (
                        <>
                            <CategoriesItem categoryId={item} key={index}/>
                        </>
                ))
            }
                           
        </div>
    )
}

export default Categories