import classNames from 'classnames';
import React, { useEffect, useState } from 'react'
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import * as yup from "yup";
import ganador from '../../../adapters/axios.config'
import CallbackCSS from './CallbackForm.module.css'

export const CallbackForm = ({showForm, setShowForm}) => {

    let schema = yup.object().shape({
        name: yup.string().required("Введите имя"),
        phone: yup.number().required("Неверный номер телефона"),
    });


    const [results,setResults] = useState({
        name: null,
        phone: null
    })

    const [formErrors, setFormErrors] = useState({})
  
    const [confirm, setConfirm] = useState(false)

    const onChangeRegForm = (arg , e) => {
        setResults(oldState => {
            return {...oldState, [arg]: e}
        })
    }

    const onSubmitHandler = (e) => {
        e.preventDefault()
        e.stopPropagation()
        schema.isValid(results).then(valid => {
            if (valid) {
                setConfirm(!confirm)
                setFormErrors({})
                const body = { 
                    name: results.name,
                    phone: results.phone
                 };
                 ganador.post('/call_feedback/', body)
                .then(response => console.log(response));
              

            }
        })
        schema.validate(results, {abortEarly: false}).catch(err => {
            err.inner.forEach(err => {
                setFormErrors(oldErrors => {
                    return {...oldErrors, [err.path]: err.message}
                })
            })
        })

        
    }
   
    return (
        <div className={classNames(showForm ? CallbackCSS.callback_form_active : CallbackCSS.callback_form) }>
            <form className={CallbackCSS.callback_form_block} onSubmit={(e) => onSubmitHandler(e)}>
                <p className={CallbackCSS.callback_form_title}>Оставьте свои контактные данные</p>
                <div className={CallbackCSS.callback_form_input}>
                    <input 
                        className={CallbackCSS.input}
                        type="text" 
                        placeholder="Name"
                        onChange={(e) => onChangeRegForm("name",e.target.value)}
                    />
                </div>
                <div className={CallbackCSS.callback_form_input}>
                <small className={classNames(formErrors.phone ? CallbackCSS.error : CallbackCSS.hide)}>{formErrors.phone}</small>
                        <PhoneInput
                            country={'ru'}
                            name={"phone"}
                            inputProps={{
                                name: "phone",
                                maxLength: 20,
                            }}
                            value={results.phone}
                            onChange={(value) => onChangeRegForm("phone",value)}
                     
                        />
                </div>
                    {
                         results.name && results.phone !== null 
                         ?
                         <div className={CallbackCSS.callback_form_btn} onClick={() => setShowForm(false)}>
                          <button>Жду звонка</button>
                        </div>
                        :
                        <div className={classNames(CallbackCSS.callback_form_btn,CallbackCSS.disabled)} >
                            <button>Жду звонка</button>
                        </div>
                        
                    }
            </form>
        </div>
    )
}