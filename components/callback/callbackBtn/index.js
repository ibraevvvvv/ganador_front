import React from 'react'
import { faPhone,faTimes  } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FaWhatsapp } from 'react-icons/fa';
import { useSelector } from 'react-redux';
export const CallbackBtn = ({showForm,setShowForm}) => {
    const whatsapp = useSelector(({social}) => social.items.whatsapp );
    return (
        <div>
            <div type="button" 
                  className={showForm ? "callback-bt whats" : "callback-bt"} 
                 >
                <div className="text-call">
                    <a href={whatsapp}>
                        <FaWhatsapp className="wa"/>
                    </a>
                </div>
            </div>
            <div type="button" 
                 className={showForm ? "callback-bt active" : "callback-bt"} 
                    onClick={() => setShowForm(!showForm)}>
                <div className="text-call">
                    <i className="fa fa-phone">
                        <FontAwesomeIcon icon={faPhone} />
                    </i>
                </div>
            </div>
            <i className="fab fa-whatsapp"></i>
            <div type="button" 
                 className="callback-bt cancel"
                 onClick={() => setShowForm(!showForm)}
                 >
                <div className="text-call">
                    <i className="fas fa-times">
                        <FontAwesomeIcon width="25px" icon={faTimes} />
                    </i>
                </div>
            </div>
        </div>
    )
}