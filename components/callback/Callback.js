import React from 'react'
import { CallbackBtn } from './callbackBtn'
import { CallbackForm } from './callbackForm'

export const Callback = () => {
    const [showForm,setShowForm] = React.useState(false)
    return (
        <div className="callbackWrapper">
           <CallbackBtn 
                showForm={showForm} 
                setShowForm={setShowForm}
            />
           <CallbackForm showForm={showForm} setShowForm={setShowForm}/>
        </div>
    )
}