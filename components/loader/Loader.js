import React from 'react';
import loaderCSS from './loader.module.css'

const Loader = () => {
    return (
        <div className={loaderCSS.loader}>
            <svg className={loaderCSS.circular}>
                <circle className={loaderCSS.path} cx="50" cy="50" r="20" fill="none" strokeWidth="5"
                        strokeMiterlimit="10"></circle>
            </svg>
        </div>
    );
};

export default Loader;
