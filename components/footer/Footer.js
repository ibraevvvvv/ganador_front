import React,{useEffect} from "react";
import axios from "axios";
import FooterCSS from './Footer.module.css'
import Link from 'next/link'
import classNames from "classnames";
import {useSelector,useDispatch} from "react-redux";
import { fetchSocials } from "../../store/actions/social.action";
import { fetchContacts } from "../../store/actions/contacts.action";
import Image from 'next/image'


const Footer = () => {
    const dispatch = useDispatch();
    const items = useSelector(({social}) => social.items );
    const isLoaded = useSelector(({social}) => social.isLoaded);
    const info = useSelector(({contacts}) => contacts.items );
    console.log(info)

    useEffect(() => {
        dispatch(fetchSocials())
        dispatch(fetchContacts())
    },[]);

    return(
        <footer className={FooterCSS.footer}>
            <div className="container">
                <div className={FooterCSS.footer__blocks}>
                    <div className={FooterCSS.footer__blocks_item}>
                        <div className={FooterCSS.footer__blocks_item_inner}>
                            <div className={classNames(FooterCSS.footer__blocks_item_inner, FooterCSS.title)}>
                                 <p>Карта сайта</p>
                            </div>
                            <div className={FooterCSS.footer__blocks_item_inner}>
                                <Link href={"/"}>
                                    <a href="#">Главная</a>
                                </Link>
                            </div>
                            <div className={FooterCSS.footer__blocks_item_inner}>
                                <Link href={"/about-us"}>
                                    <a href="#">О нас</a>
                                </Link>
                            </div>
                            <div className={FooterCSS.footer__blocks_item_inner}>
                                <Link href={"/contacts"}>
                                    <a href="#">Контакты</a>
                                </Link>
                            </div>
                        </div>
                     
                    </div>
                    <div className={FooterCSS.footer__blocks_item}>
                        <div className={classNames(FooterCSS.footer__blocks_item_inner, FooterCSS.title)}>
                            <p>Адрес</p>
                        </div>
                        <div className={FooterCSS.footer__blocks_item_inner}>
                            <p>{info?.address}</p>
                        </div>
                    </div>
                    <div className={FooterCSS.footer__blocks_item}>
                        <div className={classNames(FooterCSS.footer__blocks_item_inner, FooterCSS.title)}>
                            <p>Контакты</p>
                        </div>
                        <div className={FooterCSS.footer__blocks_item_inner}>
                            <p>Whats App</p>
                            <p>{info?.phone1}</p>
                        </div>
                        {
                            items.mail &&  
                            <div className={FooterCSS.footer__blocks_item_inner}>
                            <p>По общим вопросам:</p>
                            <p>{items?.mail}</p>
                        </div>
                        }
                        
                    </div>
                    <div className={FooterCSS.footer__blocks_item}>
                        <div className={classNames(FooterCSS.footer__blocks_item_inner, FooterCSS.title)}>
                            <p>Социальные сети</p>
                        </div>
                        <div className={FooterCSS.footer__blocks_item_inner}>
                            <div className={FooterCSS.footer__blocks_item_inner_icon}>
                            <div className={FooterCSS.one}>
                                <a href={items?.instagram}>
                                    <img  src="/images/icon-instagram.png" alt="instagram" style={{width: '40px', height: '40px'}}/>
                                </a>
                            </div>
                            <div className={FooterCSS.two}>
                                <a href={items?.vkontakte}>
                                    <img   src="/images/icon-vk.png" alt="vk" style={{width: '40px', height: '40px'}}/>
                                </a>
                            </div>
                            </div>
                            
                        </div>
                    </div>
            </div>
            </div>
            
        </footer>
    )
}

export default Footer