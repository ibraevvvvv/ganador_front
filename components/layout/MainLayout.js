import Head from "next/head";
import Link from "next/link";
import Header from "../header/Header";
import  Footer  from '../footer/Footer'

export function MainLayout({ children  ,meta_title,meta_descr,meta_key}) {
    return (
        <>
            <Head>
                <title>{'Ganador'}</title>
                <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" /> 
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
                <link rel="shortcut icon" href="/images/logo_head.png" type="image/png"/>
                <meta charset="UTF-8" />
                <meta name="description" content={meta_descr} />
                <meta name="keywords" content={meta_key} />
                <meta name="title" content={meta_title} />
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"
                />
                <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
                <meta name="theme-color" content="#ffffff" />
            </Head>
            <Header/>
            <main>{children}</main>
            <Footer />
        </>
    );
}
