import React from 'react'

// eslint-disable-next-line react/display-name
export const MapsWrapper = React.memo(
    () => {
        return <div id="map-container" style={{ width: '100%', height: '100%' }}></div>;
    },
    () => true,
);