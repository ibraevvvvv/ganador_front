import ContactsCSS from '../ContactsComponent.module.css'
import {useSelector} from "react-redux";
import { YMaps,Map ,Placemark} from 'react-yandex-maps';

const ContactsMaps = () => {
    const styles = {
        width: "100%",
        height: "100%",
        style: "border:0",
        allowfullscreen: "",
        loading: "lazy",
    };

    const items = useSelector(({contacts}) => contacts.items );
    const location = (items.geolocation)?.split(',') || []
    const x = location[0]
    const y = location[1]
    return(
       <div className={ContactsCSS.contacts_maps}>
           <YMaps style={styles}>
               <div style={styles}>
                   <Map
                       style={styles}
                       defaultState={{
                           center: [42.87, 74.59],
                           zoom: 11,
                       }}
                   >
                               <>
                                   <Placemark
                                       style={styles}
                                       modules={["geoObject.addon.balloon"]}
                                       defaultGeometry={[x, y]}
                                   />
                               </>
                   </Map>
               </div>
           </YMaps>

       </div>
    )
}

export default ContactsMaps