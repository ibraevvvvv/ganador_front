import React from 'react'
import ContactsMaps from './contactsMaps'
import ContactsText from './contactsText'
import contactsCSS from './ContactsComponent.module.css'

const ContactsComponent = () => {
    return(
            <div className="container">
                <div className={contactsCSS.contacts}>
                  <ContactsText />
                  <ContactsMaps/>
                </div>
            </div>
    )
}

export default ContactsComponent