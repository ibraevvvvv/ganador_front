import ContactsCSS from '../ContactsComponent.module.css'
import {useSelector} from "react-redux";
import Image from 'next/image'

const ContactsText = () => {
  const items = useSelector(({contacts}) => contacts.items );
   
    return(
       <div className={ContactsCSS.contacts_text}>
           <div className={ContactsCSS.contacts_text_inner} >
              <div className={ContactsCSS.contacts_text_inner_item}>
                  <h1>Контакты</h1>
              </div>
               <div>
                  <div className={ContactsCSS.contacts_text_inner_item}>
                     <span>Телефон:</span>
                     <p  className={ContactsCSS.p}>{items?.phone1}</p>
                     <p>{items?.phone2}</p>
                     <p>{items?.phone3}</p>
                  </div>
                  <div className={ContactsCSS.contacts_text_inner_item}>
                     <span>Режим работы:</span>
                     <p  className={ContactsCSS.p}>{items?.work_time}</p>
                  </div>
                  <div className={ContactsCSS.contacts_text_inner_item}>
                     <span>Адрес:</span>
                     <p  className={ContactsCSS.p}>{items?.address}</p>
                  </div>
                  <div className={ContactsCSS.contacts_text_inner_item}>
                        <a href={items.map_url}>
                           {items.map_logo ?
                              <Image
                                 width={'50px'}
                                 height={'50px'}
                                 src={items.map_logo}
                                 alt="map"
                                 className="maps"
                              />
                              : null
                           }
                        </a>
                  </div>
                  <div>
               </div>
            
               </div>
           </div>
       </div>
    )
}

export default ContactsText