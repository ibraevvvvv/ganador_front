const initialState = {
    items: [],
    isLoaded: false,
};

const mainPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_MAIN_PAGE":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };

        default:
            return state;
    }
};

export default mainPageReducer;
