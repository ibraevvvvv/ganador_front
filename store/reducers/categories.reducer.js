const initialState = {
    items: [],
    selectedItems: {},
    isLoaded: false,
};

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CATEGORIES":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };
            case "SET_CATEGORY":
                return{
                    ...state,
                    selectedItems: action.payload
                }
        default:
            return state;
    }
};

export default categoriesReducer;
