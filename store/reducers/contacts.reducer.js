const initialState = {
    items: [],
    isLoaded: false,
};

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CONTACTS":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };

        default:
            return state;
    }
};

export default contactsReducer;
