const initialState = {
    items: [],
    isLoaded: false,
};

const categoryIdReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CATEGORY_ID":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };
        default:
            return state;
    }
};

export default categoryIdReducer;
