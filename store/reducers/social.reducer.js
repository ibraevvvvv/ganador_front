const initialState = {
    items: [],
    isLoaded: false,
};

const socialsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_SOCIALS":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };

        default:
            return state;
    }
};

export default socialsReducer;
