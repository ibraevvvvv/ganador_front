const initialState = {
    items: [],
    isLoaded: false,
};

const clothesIdReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CLOTHES_ID":
            return {
                ...state,
                items: action.payload,
                isLoaded: true,
            };
        case "SET_ACTIVE":
            return {
                ...state,
                items: [...state.items,]
            }
        case "SET_LOADED":
            return {
                ...state,
                isLoaded: action.payload,
            };
        default:
            return state;
    }
};

export default clothesIdReducer;
