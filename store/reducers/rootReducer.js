import { combineReducers } from "redux";
import aboutReducer from './about.reducer'
import categoriesReducer from "./categories.reducer";
import categoryIdReducer from "./categoryId.reducer";
import clothesIdReducer from "./clothes.reducer";
import contactsReducer from "./contacts.reducer";
import mainPageReducer from "./mainPage.reducer";
import socialsReducer from "./social.reducer";

const rootReducer = combineReducers({
    about: aboutReducer,
    contacts: contactsReducer,
    category: categoriesReducer,
    social: socialsReducer,
    categoryId: categoryIdReducer,
    clothesId: clothesIdReducer,
    mainPage: mainPageReducer
});

export default rootReducer;
