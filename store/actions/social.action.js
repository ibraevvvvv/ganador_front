import ganador from '../../adapters/axios.config';

export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getSocials = (items) => ({
    type: "GET_SOCIALS",
    payload: items,
});


export const fetchSocials = () => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/social-networks`)
            .then(({ data }) => {
                dispatch(getSocials(data));
            });
    }catch (e) {
        console.log(e)
    }
};
