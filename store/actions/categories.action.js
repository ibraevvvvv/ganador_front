import ganador from '../../adapters/axios.config';
export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getCategories = (items) => ({
    type: "GET_CATEGORIES",
    payload: items,
});


export const fetchCategories = () => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/category`)
            .then(({ data }) => {
                dispatch(getCategories(data));
            });
    }catch (e) {
        console.log(e)
    }
};
