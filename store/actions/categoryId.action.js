import ganador from '../../adapters/axios.config';
export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getCategoryId = (items) => ({
    type: "GET_CATEGORY_ID",
    payload: items,
});


export const fetchCategoryId = (slug) => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/category/${slug}`)
            .then(({ data }) => {
                dispatch(getCategoryId(data));
            });
    }catch (e) {
        console.log(e)
    }
};
