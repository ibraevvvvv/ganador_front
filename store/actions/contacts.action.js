import ganador from '../../adapters/axios.config';

export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getContacts = (items) => ({
    type: "GET_CONTACTS",
    payload: items,
});


export const fetchContacts = () => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/information`)
            .then(({ data }) => {
                dispatch(getContacts(data));
            });
    }catch (e) {
        console.log(e)
    }
};
