import ganador from '../../adapters/axios.config';

export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getMainPage = (items) => ({
    type: "GET_MAIN_PAGE",
    payload: items,
});

export const fetchMainPage = () => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/main_page`)
            .then(({ data }) => {
                dispatch(getMainPage(data));
            });
    }catch (e) {
        console.log(e)
    }
};
