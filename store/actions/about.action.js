import ganador from '../../adapters/axios.config';
export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getAboutUS = (items) => ({
    type: "GET_ABOUT_US",
    payload: items,
});


export const fetchAboutUs = () => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/about_us`)
            .then(({ data }) => {
                dispatch(getAboutUS(data));
            });
    }catch (e) {
        console.log(e)
    }
};
