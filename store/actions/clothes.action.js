import ganador from '../../adapters/axios.config';

export const setLoaded = (payload) => ({
    type: "SET_LOADED",
    payload,
});

const getClothesId = (items) => ({
    type: "GET_CLOTHES_ID",
    payload: items,
});


export const fetchClothesId = (id) => (dispatch) => {
    dispatch(setLoaded());
    try {
        ganador
            .get(`/clothes/${id}`)
            .then(({data}) => {
                dispatch(getClothesId(data));
            });
    } catch (e) {
        console.log(e)
    }
};
