import React from 'react'
import CardCategories from '../components/home/cardCategories'
import { MainLayout } from '../components/layout/MainLayout'
import NewCollectionBlock from '../components/home/newCollectionBlock'
import NewCollectionBlockTwo from '../components/home/newCollectionBlockTwo'
import { Callback } from '../components/callback/Callback'
import {useSelector,useDispatch} from "react-redux";
import { fetchMainPage } from '../store/actions/mainPage.action'
import NewCollectionBlockThree from "../components/home/newCollectionBlockThree";


export default function Home() {

  const dispatch = useDispatch();
  const banners = useSelector(({mainPage}) => mainPage.items);
  const lastBanner = useSelector(({mainPage}) => mainPage.items.last_banner);

  React.useEffect(() => {
      dispatch(fetchMainPage())
  },[]);

  return (
    <MainLayout
        meta_title={banners?.main_banner?.meta_title}
        meta_descr={banners?.main_banner?.meta_description}
        meta_key={banners?.main_banner?.meta_keywords}
        >
          <NewCollectionBlock/>
          <CardCategories/>
          {
            banners?.banners?.map((item,index) => (
              <NewCollectionBlockTwo item={item} key={index}/>
            ))
          }
          <NewCollectionBlockThree item={lastBanner}/>
          <Callback/>
    </MainLayout>
  )
}
