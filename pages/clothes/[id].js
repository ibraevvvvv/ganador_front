import React,{useEffect} from 'react'
import ClothesComponent from '../../components/clothes/ClothesComponent'
import { MainLayout } from "../../components/layout/MainLayout"
import { fetchClothesId } from '../../store/actions/clothes.action'
import {useSelector,useDispatch} from "react-redux";
import Loader from "../../components/loader/Loader";
import {Callback} from "../../components/callback/Callback";

const Clothes = () => {
    const dispatch = useDispatch();
    const isLoaded = useSelector(({clothesId}) => clothesId.isLoaded);

    let path
    useEffect(() => {
        path = window.location.pathname.split('/').pop()
        dispatch(fetchClothesId(path))
    },[]);

    return(
        <MainLayout>
            {isLoaded ?  <ClothesComponent/> : <Loader/>}
            <Callback/>
        </MainLayout>
    )
}

export default Clothes