import React,{useEffect} from "react"
import AboutUsComponent from "../components/aboutUs/AboutUsComponent"
import { MainLayout } from "../components/layout/MainLayout"
import {useSelector,useDispatch} from "react-redux";
import { fetchAboutUs } from "../store/actions/about.action";
import {Callback} from "../components/callback/Callback";

const AboutUs = () => {    

    const dispatch = useDispatch();
    const items = useSelector(({about}) => about.items );
    const isLoaded = useSelector(({about}) => about.isLoaded);

    useEffect(() => {
        dispatch(fetchAboutUs())
    },[]);

    return(
        <MainLayout 
            meta_title={items.meta_title}
            meta_descr={items.meta_description} 
            meta_key={items.meta_keywords}
            >
            <AboutUsComponent/>
            <Callback/>
        </MainLayout>
    )
}

export default AboutUs