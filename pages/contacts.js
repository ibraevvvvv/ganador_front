import React,{useEffect} from "react"
import { MainLayout } from "../components/layout/MainLayout"
import ContactsComponent from "../components/contacts/ContactsComponent"
import { fetchContacts } from "../store/actions/contacts.action"
import {useSelector,useDispatch} from "react-redux";
import {Callback} from "../components/callback/Callback";


const Contacts = () => {
    const dispatch = useDispatch();
    const items = useSelector(({contacts}) => contacts.items );
    const isLoaded = useSelector(({contacts}) => contacts.isLoaded);

    useEffect(() => {
        dispatch(fetchContacts())
    },[]);
    
    return(
        <MainLayout 

            meta_title={items.meta_title} 
            meta_descr={items.meta_description} 
            meta_key={items.meta_keywords}
            >
                <ContactsComponent />
            <Callback/>
        </MainLayout>
    )
}

export default Contacts