import React,{useEffect} from "react"
import { MainLayout } from "../../components/layout/MainLayout"
import Categories from "../../components/categories/Categories"
import {useSelector,useDispatch} from "react-redux";
import { fetchCategoryId } from "../../store/actions/categoryId.action";
import Loader from "../../components/loader/Loader";
import {Callback} from "../../components/callback/Callback";

const Category = () => {

    const dispatch = useDispatch();
    const categoryId = useSelector(({categoryId}) => categoryId.items );
    const isLoaded = useSelector(({categoryId}) => categoryId.isLoaded);
    
    let path 
    useEffect(() => {
        path = window.location.pathname.split('/').pop()
        dispatch(fetchCategoryId(path))
    },[]);

    return(
        <MainLayout>
               <div className="container">
                   {isLoaded ?   <Categories categoryId={categoryId}/> : <Loader/>}
                   <Callback/>
               </div>
        </MainLayout>
    )
}

export default Category